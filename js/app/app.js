var app = angular.module('messageWall',["firebase"]);

app.controller('messageParser',function($firebase){
    var messages =  new Firebase("https://tweetzerk.firebaseio.com/");
    this.messages = $firebase(messages);
    
});

app.controller('messageForm',function($firebase){
    var messages = new Firebase("https://tweetzerk.firebaseio.com/");
    this.messages = $firebase(messages);
    
    this.sendMessage = function(messageText,fbusername){
        var message = {
            "text":messageText,
            "user":{
                "name":fbusername,
                "picture" : "https:/graph.facebook.com/"+fbusername+"/picture"
            }
        };
        this.messages.$add(message);
    }
});
